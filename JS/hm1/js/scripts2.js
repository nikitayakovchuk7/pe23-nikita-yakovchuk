
let name = prompt('What is your name?')
while (name === null || name.trim() === ''){
    name = prompt('What is your name?')
}
let ageCorrect = true;

do {
    let age = +prompt("How old are you?");
    if (age >= 1 && age < 18) {
        alert('You are not allowed to visit this website.');
    }  else if (age >= 18 && age <= 22) {
        const userAge = confirm('Are you sure you want to continue?')
        if(userAge === true){
        alert('Welcome, ' + name)
            } else{
            alert('You are not allowed to visit this website.')
        }}
    else if(age>22) {
            alert('Welcome, ' +name)
        }
    else {
            ageCorrect = false
        }

} while (ageCorrect === false);

// 1. Отличие перменных let и const от var - блочная видимость, то есть переменные существуют только в блоке, в котором их обьявили.
// Переменные let и const видны только после того, как будут обьявлены.
// let и const нельзя переобъявлять (в том же блоке).

// 2. Большая часть современных кодов пишеться по стандартам ES-2015, где переменная var не принята. Не практичен в виду отсутствия блочной видимости и не может быть локальным для функций и циклов.
