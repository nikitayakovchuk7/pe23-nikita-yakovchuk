function filterBy(arr, type){
    let newArray = []
    arr.forEach(function (item) {
        if (typeof item !== type){
            newArray.push(item)
        }})
    return newArray
}

let array = [150, 10, 15,  undefined, true, false, null, "pass", "asds", 33];
let newType = prompt('Enter type')

console.log(filterBy(array, newType));