function createNewUser() {
    let firstName = prompt('Enter first name:');
    let lastName = prompt('Enter last name:');
    let user = {
        getLogin: function () {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        }}
    Object.defineProperties(user, {
        firstName: {
            value: firstName
        },
        lastName: {
            value: lastName
        }
    });
    return user;
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
